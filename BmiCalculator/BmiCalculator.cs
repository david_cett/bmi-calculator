﻿namespace BmiCalculator
{
    public class BmiCalculator:IBmiCalculator
    {
        public double GetBmi(double weight,double height)
        {
            return weight /(height * height);
        }

        public string GetClassification(double bmi)
        {
            if (bmi < 18.5)
                return "Sottopeso";
            if (bmi >= 18.5 && bmi < 25)
                return "Normale";
            if (bmi >= 25 && bmi <= 29)
                return "Sovrappeso";
            else
                return "Obeso";
        }
    }
}
