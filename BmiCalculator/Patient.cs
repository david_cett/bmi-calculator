﻿using System.Collections.Generic;

namespace BmiCalculator
{
    public class Patient
    {
        private readonly BmiCalculator _bmiCalculator;

        public string Name { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }

        public Patient()
        {

        }
        public Patient(BmiCalculator bmiCalculator)
        {
            _bmiCalculator = bmiCalculator;
        }

        public Patient(string name, double weight,double height,BmiCalculator bmiCalculator)
        {
            Name = name;
            Weight = weight;
            Height = height;
            _bmiCalculator = bmiCalculator;
        }


        public static IList<List<string>> GetPatientsWithBmiList(List<Patient> patients)
        {
            var list = new List<List<string>>();
            foreach (var patient in patients)
            {
                var patientList = new List<string>();
                patientList.Add(patient.Name);
                patientList.Add(patient.GetBmi().ToString());
                patientList.Add(patient.GetClassification());
                list.Add(patientList);
            }
            return list;
        }

        public double GetBmi()
        {
            return _bmiCalculator.GetBmi(Weight, Height);
        }

        public string GetClassification()
        {
            return _bmiCalculator.GetClassification(GetBmi());
        }
    }

}
