﻿using Ninject.Modules;
using Quartz;

namespace BmiCalculator
{
    public class ActionModule : NinjectModule
    {      
        public override void Load()
        {
            Kernel.Bind<IAction>().To<Action>().InSingletonScope();

        }
    }

}
