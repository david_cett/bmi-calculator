﻿using Ninject.Modules;
using System.Configuration;

namespace BmiCalculator
{
    public class FileModule : NinjectModule
    {
        private  string path =ConfigurationManager.AppSettings["path"];
        private string inputPath = ConfigurationManager.AppSettings["inputPath"];
        private string outputPath = ConfigurationManager.AppSettings["outputPath"];

        public override void Load()
        {
            Kernel.Bind<IFileCsv>().To<FileCsv>().WithConstructorArgument("path",path)
                .WithConstructorArgument("inputpath", inputPath)
                .WithConstructorArgument("outputPath", outputPath); 
           
        }
    }

}
