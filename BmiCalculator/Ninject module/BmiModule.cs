﻿using Ninject.Modules;

namespace BmiCalculator
{
    public class BmiModule : NinjectModule
    {      
        public override void Load()
        {
            Kernel.Bind<IBmiCalculator>().To<BmiCalculator>();
        }
    }
    
}
