﻿using System.Collections.Generic;

namespace BmiCalculator
{
    public interface IFileCsv
    {
        IList<dynamic> ReadFileCsv();
        void WriteFileCsv(IList<List<string>> lists);
    }
}
