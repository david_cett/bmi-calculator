﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace BmiCalculator
{
    public class FileCsv:IFileCsv
    {
        private readonly string _path;
        private readonly string _inputPath;
        private readonly string _outputPath;

        public FileCsv(string path,string inputpath,string outputPath)
        {
            _path = path;
            _inputPath = inputpath;
            _outputPath = outputPath;
        }


        public IList<dynamic> ReadFileCsv()
        {
            using (var reader = new StreamReader(_path + _inputPath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                //csv.Configuration.HasHeaderRecord = false;
                return csv.GetRecords<dynamic>().ToList();
            }

            //var lists = new List<List<string>>();


            //using (var reader = new StreamReader(_path + _inputPath))
            //{
            //    while (!reader.EndOfStream)
            //        lists.Add(reader.ReadLine().Split(new char[] { ',' }).ToList());
            //}
            //return lists;
        }



        public void WriteFileCsv(IList<List<string>> lists)
        {
            var path = _path+_outputPath + $@"bmi-{DateTime.Now.ToString("yyyy-M-dd -- HH-mm-ss")}.csv";
            var sb = new StringBuilder();
            foreach (var line in lists)
            {    
                sb.Append(string.Join(",",line));               
                sb.Append(Environment.NewLine);
            }
            File.AppendAllText(path, sb.ToString());
        }
    }
}
