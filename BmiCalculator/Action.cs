﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BmiCalculator
{
    public class Action:IAction
    {
        private readonly IFileCsv _fileCsv;
        private readonly BmiCalculator _bmiCalculator;       

        public Action(IFileCsv fileCsv,BmiCalculator bmiCalculator)
        {
            _fileCsv = fileCsv;
            _bmiCalculator = bmiCalculator;          
        }

        public void GetBmiCalculator()
        {
            var lists = _fileCsv.ReadFileCsv().ToList();
            
            var patients = lists.Select(p =>
            new Patient
            {
                Name = ((IDictionary<String, Object>)p)["Name"].ToString(),
                Weight = Convert.ToDouble(((IDictionary<String, Object>)p)["Weight"]),
                Height = Convert.ToDouble(((IDictionary<String, Object>)p)["Height"])

            });
            _fileCsv.WriteFileCsv(Patient.GetPatientsWithBmiList(patients.ToList()));

        }

    }
   
}
