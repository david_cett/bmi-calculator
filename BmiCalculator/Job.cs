﻿using Ninject;
using Quartz;
using System;
using System.Threading.Tasks;


namespace BmiCalculator
{
    public class Job : IJob
    {
        private readonly IAction _action;

        public Job(IAction action)
        {
            _action = action;

        }

        public void Execute(IJobExecutionContext context)
        {
            Console.WriteLine("bmi-calculator");
            _action.GetBmiCalculator();
        }
    }

}
