﻿namespace BmiCalculator
{
    public interface IBmiCalculator
    {
        double GetBmi(double weight, double height);
        string GetClassification(double bmi);
    }

}
