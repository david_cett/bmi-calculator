﻿using Ninject;
using Quartz;
using Quartz.Impl;
using System;
using System.Threading.Tasks;
using Topshelf;
using Topshelf.Ninject;
using Topshelf.Quartz;


namespace BmiCalculator
{
    class Program
    {
        static void Main(string[] args)
        {         
            HostFactory.Run(c =>
            {
                             
                c.SetServiceName("bmi-calculator");
                c.SetDisplayName("bmi-calculator");
                c.SetDescription("bmi-calculator");

                c.UseNinject(new FileModule(), new BmiModule(), new ActionModule());
                c.UsingQuartzJobFactory(() => new NinjectJobFactory(NinjectBuilderConfigurator.Kernel));

                c.Service<Service>(s =>
                {
                    s.ConstructUsingNinject();
                    s.WhenStarted(service => service.Start());

                    s.ScheduleQuartzJob(q =>
                        q.WithJob(() =>
                            JobBuilder.Create<Job>().Build())
                            .AddTrigger(() => TriggerBuilder.Create()
                                    .WithSimpleSchedule(b => b
                                        .WithIntervalInSeconds(30)
                                        .RepeatForever())
                                    .Build()));

                    s.WhenStopped(service => service.Stop());
                });

                c.StartAutomatically();
                c.RunAsLocalService();
                c.EnableShutdown();

            });
          
        }
    }
 
}
